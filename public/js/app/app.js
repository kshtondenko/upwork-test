(function() {
    'use strict';

    angular
        .module('invoicing.app', [
            'ui.router',
            'ngResource',
            'isteven-multi-select'
        ])
        .config(config)
        .run(run);

    config.$inject = [ '$stateProvider', '$urlRouterProvider' ];

    function config($stateProvider, $urlRouterProvider){
        $urlRouterProvider.otherwise('/home');

        $stateProvider

            // HOME STATES AND NESTED VIEWS ========================================
            .state('home', {
                url: '/home',
                templateUrl: 'main/main.html',
                controller: 'MainCtrl',
                controllerAs: 'vm'
            })

            // ABOUT PAGE AND MULTIPLE NAMED VIEWS =================================
            .state('about', {
                // we'll get to this in a bit       
            });
    };

    run.$inject = ['$rootScope'];

    function run($rootScope){
        $rootScope.test = 'test';
    };

    angular
        .module('invoicing.app')
        .directive('invoicesList', invoicesList);

        invoicesList.$inject = [];

        function invoicesList() {
            var directive = {
                bindToController: true,
                controller: 'InvoicesListCtrl',
                controllerAs: 'vm',
                templateUrl: 'invoices.list/invoices.list.html',
                link: link,
                restrict: 'AE',
                scope: {
                }
            };
            return directive;

            function link(scope, element, attrs) {
                
            }
        };

    angular
        .module('invoicing.app')
        .factory('InvoicesList', InvoicesList);

        InvoicesList.$inject = ['$resource'];

        function InvoicesList($resource) {
            return $resource('/api/', {}, {
                getInvoices: { method: 'GET', url: '/api/invoices', isArray: true},
                getCustomers: { method: 'GET', url: '/api/customers', isArray: true},
                getProducts: { method: 'GET', url: '/api/products', isArray: true},
                createInvoice: { method: 'POST', url: '/api/invoices'}
            });
        }

    angular
        .module('invoicing.app')
        .controller('InvoicesListCtrl', InvoicesListCtrl);

        InvoicesListCtrl.$inject = ['InvoicesList'];

        function InvoicesListCtrl(InvoicesList) {
            var vm = this;
            vm.invoiceTotal = 0;
            vm.invoiceDiscount = 0;

            vm.invoices = InvoicesList.getInvoices(function(resp){
            });

            vm.customers = InvoicesList.getCustomers(function(resp){
            });

            vm.products = InvoicesList.getProducts(function(resp){
            });
            
            vm.calculateTotal = function() {
                var total = 0;
                angular.forEach(vm.selectedProducts, function(product) {
                    total += product.price * product.quantity;
                });
                console.log(total);
                total = total * (1 - vm.invoiceDiscount/100);
                console.log(total);

                vm.invoiceTotal = total;
            };
        };

    angular
        .module('invoicing.app')
        .controller('MainCtrl', MainCtrl);

        MainCtrl.$inject = [];

        function MainCtrl() {
            var vm = this;
        };

})();

